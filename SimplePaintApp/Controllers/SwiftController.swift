//
//  ViewController.swift
//  SimplePaintApp
//
//  Created by Petr Lzicar on 08.01.2022.
//

import UIKit

class SwiftController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    var lastPoint:CGPoint?
    
    override func viewDidLayoutSubviews() {
        if let imageSize = imageView.image?.size {
            let scaledSize = CGSize.computeScale(canvasSize: self.view.frame.size, imageSize: imageSize)
            imageWidth.constant = scaledSize.width
            imageHeight.constant = scaledSize.height
            self.view.layoutIfNeeded()
        }        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func handlePan(_ gesture: UIPanGestureRecognizer) {
        let touchLocation = gesture.location(in: imageView)
        let canvasSize = CGSize(width: self.imageView.frame.width, height: self.imageView.frame.height)
        
        let changedImage = Painting.draw(
            image: imageView.image!,
            canvasSize: canvasSize,
            fromPoint: lastPoint ?? touchLocation,
            toPoint: touchLocation
        )
        lastPoint = touchLocation
        if (gesture.state == .ended) {
            lastPoint = nil
        }
        self.imageView?.image  = changedImage
    }
    
   
    
}



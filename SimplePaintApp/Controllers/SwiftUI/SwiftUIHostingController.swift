//
//  SwiftUIHostingController.swift
//  SimplePaintApp
//
//  Created by Petr Lzicar on 08.01.2022.
//

import Foundation
import SwiftUI


class SwiftUIHostingController: UIHostingController<PaintSwiftUIView> {
    
    required init?(coder: NSCoder) {
           super.init(coder: coder,rootView: PaintSwiftUIView());
       }

       override func viewDidLoad() {
           super.viewDidLoad()
       }
}

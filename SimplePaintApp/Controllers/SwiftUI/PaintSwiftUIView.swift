//
//  SwiftUI.swift
//  SimplePaintApp
//
//  Created by Petr Lzicar on 08.01.2022.
//

import SwiftUI

struct PaintSwiftUIView: View {
    @State var image = UIImage(named:"Bear")
    @State var imageSize: CGSize = .zero
    @State var lastPoint: CGPoint? = nil
    
    var body: some View {
        Image(uiImage: self.image!)
            .resizable(resizingMode: .stretch)
            .aspectRatio(contentMode: .fit)
            .background(calculateImageViaBackground())
            .gesture(
                DragGesture()
                    .onChanged{ gesture in
                        self.image = Painting.draw(
                            image: self.image!,
                            canvasSize: self.imageSize,
                            fromPoint: lastPoint ?? gesture.location,
                            toPoint: gesture.location
                        )
                        
                        self.lastPoint = gesture.location
                    }.onEnded { gesture in
                        self.lastPoint = nil
                    }
            )
    }
    
    // https://stackoverflow.com/questions/59620307/swiftui-getting-an-images-displaying-dimensions
    func calculateImageViaBackground() -> some View {
        return GeometryReader { (geometry) -> AnyView in
            let imageSize = geometry.size
            DispatchQueue.main.async {
                if (lastPoint != .zero && self.imageSize != imageSize) {
                    self.imageSize = imageSize
                }
            }
            return AnyView(Rectangle().fill(Color.clear))
        }
    }
}

//
//  RxSwiftController.swift
//  SimplePaintApp
//
//  Created by Petr Lzicar on 08.01.2022.
//

import UIKit
import RxSwift
import RxGesture

class RxSwiftController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    var image: BehaviorSubject<UIImage?> = BehaviorSubject(value: UIImage(named: "Bear"))
    let disposeBag = DisposeBag()
    var lastPoint:CGPoint?
    
    override func viewDidLayoutSubviews() {
        if let imageSize = imageView.image?.size {
            let scaledSize = CGSize.computeScale(canvasSize: self.view.frame.size, imageSize: imageSize)
            imageWidth.constant = scaledSize.width
            imageHeight.constant = scaledSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image
            .observe(on: MainScheduler.instance)
            .bind(to: imageView.rx.image)
            .disposed(by: disposeBag)
        
        imageView.rx.panGesture().when(.began).subscribe(onNext: { gesture in
            self.lastPoint = gesture.location(in: self.imageView)
        })
            .disposed(by: self.disposeBag)
        
        imageView.rx.panGesture().when(.changed)
            .subscribe(onNext: { gesture in
                let touchLocation = gesture.location(in: self.imageView)
                let canvasSize = CGSize(width: self.imageView.frame.width, height: self.imageView.frame.height)
                
                let image =  Painting.draw(
                    image: self.imageView.image!,
                    canvasSize: canvasSize,
                    fromPoint: self.lastPoint ?? touchLocation,
                    toPoint: touchLocation
                )
                self.image.onNext(image)
                
                self.lastPoint = touchLocation
            }).disposed(by: self.disposeBag)
        
    }
    
}

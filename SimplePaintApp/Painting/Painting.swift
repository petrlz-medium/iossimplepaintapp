//
//  Painting.swift
//  SimplePaintApp
//
//  Created by Petr Lzicar on 08.01.2022.
//

import UIKit


struct Painting {
    static func draw(image:UIImage, canvasSize:CGSize, fromPoint:CGPoint, toPoint:CGPoint ) -> UIImage? {
        let imageSize = image.size
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 1)
        let context = UIGraphicsGetCurrentContext()
        
        image.draw(in: CGRect(x: 0, y: 0, width:imageSize.width, height: imageSize.height))
        
        let ratioX = imageSize.width / canvasSize.width
        let ratioY = imageSize.height  / canvasSize.height
        
        
        let fromX = fromPoint.x * ratioX
        let fromY = fromPoint.y * ratioY
        let toX = toPoint.x * ratioX
        let toY = toPoint.y * ratioY
        
        let lineWidth:CGFloat = 20
        let color =  UIColor.init(red: 207/255, green: 66/255, blue: 68/255, alpha: 1)
        
        context?.move(to: CGPoint(x: fromX, y: fromY))
        context?.addLine(to: CGPoint(x: toX, y: toY))
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(lineWidth * ratioX)
        context?.setStrokeColor(color.cgColor)
        context?.setBlendMode(CGBlendMode.normal)
        context?.strokePath()
        
        let output = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return output
    }
    
}

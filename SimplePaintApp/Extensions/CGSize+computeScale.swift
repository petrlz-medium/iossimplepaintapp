//
//  Cg.swift
//  SimplePaintApp
//
//  Created by Petr Lzicar on 08.01.2022.
//

import UIKit

extension CGSize {
    static func computeScale(canvasSize: CGSize, imageSize: CGSize) -> CGSize {
        let ratioX = canvasSize.width / imageSize.width
        let ratioY = canvasSize.height / imageSize.height
        
        let ratio = ratioX < ratioY ? ratioX : ratioY
        
        return CGSize(width: imageSize.width * ratio, height: imageSize.height * ratio)
    }
}
